import time, os
from zapv2 import ZAPv2

host_pattern = os.environ.get('HOST_PATTERN', 'site.com')
scan_url_scheme = os.environ.get('SCAN_URL_SCHEME', 'http')
zap_url = os.environ.get('ZAP_URL', 'zap:8080')

target = host_pattern
apikey = 'changeme' # Change to match the API key set in ZAP, or use None if the API key is disabled

# By default ZAP API client will connect to port 8080
zap = ZAPv2(
    apikey=apikey,
    proxies={
        'http': f'http://{zap_url}',
        'https': f'http://{zap_url}'
    }
)

# Use the line below if ZAP is not listening on port 8080, for example, if listening on port 8090
# zap = ZAPv2(apikey=apikey, proxies={'http': 'http://127.0.0.1:8090', 'https': 'http://127.0.0.1:8090'})
# Proxy a request to the target so that ZAP has something to deal with

print('Accessing target {}'.format(target))
zap.urlopen(f'{scan_url_scheme}://{target}/')

# Give the sites tree a chance to get updated
print('Spidering target {}'.format(target))
scanid = zap.spider.scan(target)

# Give the Spider a chance to start
time.sleep(15)
try:
    while (int(zap.spider.status(scanid)) < 100):
        # Loop until the spider has finished
        print('Spider progress %: {}'.format(zap.spider.status(scanid)))
        time.sleep(2)
    print ('Spider completed')
except:
    print('Spider did not run')

# Passive scan start
try:
    while (int(zap.pscan.records_to_scan) > 0):
        print ('Records to passive scan : {}'.format(zap.pscan.records_to_scan))
        time.sleep(2)
    print ('Passive Scan completed')
except:
    print('Passive scan did not start')

# Active scan start

try:
    scan_list = list()
    for url in filter(lambda host: host_pattern in host, zap.core.hosts):
        print(f'New scan - {url}')
        scan_list.append(zap.ascan.scan(url=f'{scan_url_scheme}://{url}'))

    for scan in scan_list:
        old_score = 0
        same_score_counter = 0
        while int(zap.ascan.status(scan)) < 100:
            # Loop until the scanner has finished or freezed
            if same_score_counter == 20:
                break
            if old_score == zap.ascan.status(scan):
                same_score_counter += 1
            old_score = zap.ascan.status(scan)
            print(f'Scan progress %: {old_score}')
            time.sleep(5)
    print('New host active Scan completed')
except:
    print('Active scan had issues')
    
# Report the results
print ('Hosts: {}'.format(', '.join(zap.core.hosts)))
print (f'Alerts: {zap.core.alerts()}')

with open('zap_report.html', 'w') as report:
    report.write(zap.core.htmlreport())
with open('zap_report.json', 'w') as report:
    report.write(zap.core.jsonreport())
with open('zap_report.xml', 'w') as report:
	report.write(zap.core.xmlreport())
