

# FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster
FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim

RUN dotnet tool install --global Microsoft.CST.ApplicationInspector.CLI
RUN dotnet tool install --global dotnet-sonarscanner
RUN dotnet add package Microsoft.CodeAnalysis.VersionCheckAnalyzer

RUN apt update && apt upgrade -y && mkdir -p /usr/share/man/man1
RUN apt install default-jdk default-jre -y


ENV PATH="/root/.dotnet/tools:${PATH}"

# ENTRYPOINT ["appinspector"]
ENTRYPOINT []

dotnet tool install --global dotnet-sonarscanner
dotnet sonarscanner begin /k:"external-improvity-partner-backend"  /d:sonar.login="fd96e09e5628bc0811f92271dd0aa31981788c7c" /d:sonar.host.url="https://sonar.k-sec.sbermarket.tech"
dotnet build src/Improvity.IOS.sln
dotnet sonarscanner end /d:sonar.login="fd96e09e5628bc0811f92271dd0aa31981788c7c"